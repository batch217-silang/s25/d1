// console.log("Hello World!");

/*[SECTION] JSON Objects
Javascript Object Notation
Used by other programming languages, for serializing different data types
"parsing"
"stringify"
Serialization is the process of converting data into a series of bytes for easier transmission/transfer of information
SYNTAX:
{
	"propertyA" : "valueA",
	"propertyB" : "valueB"
}
*/

/*JSON object*/
// {
// 	"city" : "Quezon City",
// 	"province" : "Metro Manila",
// 	"country" : "Philippines"
// }



/*JSON Array
arrayName = cities*/

// "cities" [
// 	{
// 		"city" : "Quezon City", 
// 		"province" : "Metro Manila", 
// 		"country" : "Philippines"
// 	},
// 	{	"city" : "Manila City", 
// 		"province" : "Metro Manila", 
// 		"country" : "Philippines"
// 	},
// 	{
// 		"city" : "Makati City", 
// 		"province" : "Metro Manila", 
// 		"country" : "Philippines"
// 	}
// ]

/*JSON METHODS
-json objects contains method for parsing and converting data into stringified JSON
*/

let batchesArr = [
	{
		batchName : "Batch 217",
		schedule : "Full-Time",
	},
	{
		batchName : "Batch 218",
		schedule : "Part-Time"
	}
]

/*STRINGIFY - converting method for JS objects into a string*/

console.log("Result from stringify method:");
console.log(JSON.stringify(batchesArr));

let data = JSON.stringify({
	name: "John", 
	age: 31,
	address: {
		city: "Manila",
		country: "Philippines"
	}
});
console.log(data);

/*[SECTION] USING STRINGIFY METHOD WITH VARIABLES
*/
let firstName = prompt("What is your first name?");
let lastName = prompt("What is your last name?");
let age = prompt("What is your age?");
let address = {
	city: prompt("Which city do you live in?"),
	country: prompt("Which country does your city belong to?")
}

let otherData = JSON.stringify({
	firstName: firstName,
	lastName: lastName,
	age: age,
	address: address
});

console.log(otherData);


/*[SECTION] Converting Stringified JSON into JS objects
*/
// PARSE METHOD
let batchesJSON = `[
	{
		"batchName" : "Batch 217",
		"schedule" : "Full-Time"
	},
	{
		"batchName" : "Batch 218",
		"schedule" : "Part-Time"

	}
]`
console.log("Result from parse method:");
console.log(JSON.parse(batchesJSON));


/*STRINGIFIED ------ PARSE*/
let stringifiedObject = `{
	"name" : "John",
	"age" : 31,
	"address" : {
		"city" : "Manila",
		"country" : "Philippines"
	}
}`
console.log(JSON.parse(stringifiedObject));